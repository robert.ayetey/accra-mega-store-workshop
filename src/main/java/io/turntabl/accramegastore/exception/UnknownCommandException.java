package io.turntabl.accramegastore.exception;

public class UnknownCommandException extends RuntimeException {

    public UnknownCommandException(String msg) {
        super(msg);
    }
}
